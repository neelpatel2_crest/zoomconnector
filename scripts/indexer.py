import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import copy
import time
import json
import multiprocessing
import pandas as pd
import src.logger_manager as log
import src.constant as constant
from datetime import datetime
from src.zoom_access_token import ZOOMAccessToken
from src.checkpointing import Checkpoint
from src.configuration import Configuration
from src.base_class import BaseClass
from src.utils import print_and_log
from src.zoom_users import ZoomUsers
from src.adapter import DEFAULT_SCHEMA

logger = log.setup_logging("zoom_index")

gv_start_time = None
gv_end_time = None


class Indexer(BaseClass):
    def __init__(self, access_token, user_data):
        BaseClass.__init__(self, logger=logger)
        self.access_token = access_token
        self.is_error = False
        self.user_data = user_data

    def get_schema_fields(self, document_name):
        """Returns the schema of all the include_fields or exclude_fields specified in the configuration file.
        :param document_name: Document name from teams, channels, channel_messages, channel_tabs, channel_documents, calendar and user_chats
        Returns:
            schema: Included and excluded fields schema
        """
        fields = self.objects.get(document_name)
        adapter_schema = DEFAULT_SCHEMA[document_name]
        field_id = adapter_schema["id"]
        if fields:
            include_fields = fields.get("include_fields")
            exclude_fields = fields.get("exclude_fields")
            if include_fields:
                adapter_schema = {
                    key: val
                    for key, val in adapter_schema.items()
                    if val in include_fields
                }
            elif exclude_fields:
                adapter_schema = {
                    key: val
                    for key, val in adapter_schema.items()
                    if val not in exclude_fields
                }
            adapter_schema["id"] = field_id
        return adapter_schema

    def filter_removed_item_by_id(self, item, id):
        """This method is used filter removed document by id
        :param item: Pass document
        :param id: Pass id of the document which having error from workplace search
        """
        return item["id"] == id

    def bulk_index_document(self, document, param_name):
        """This method indexes the documents to the workplace.
        :param document: Document to be indexed
        :param success_message: Success message
        :param failure_message: Failure message while indexing the document
        :param param_name: Parameter name
        """
        try:
            document_list = []
            if document:
                total_records_dict = self.get_records_by_types(document)
                self.logger.info(
                    f"Indexing the {param_name} documents to the Workplace Search..."
                )
                document_list = [
                    document[
                        i * constant.DOCUMENT_LIMIT : (i + 1) * constant.DOCUMENT_LIMIT
                    ]
                    for i in range(
                        (len(document) + constant.DOCUMENT_LIMIT - 1)
                        // constant.DOCUMENT_LIMIT
                    )
                ]

                for chunk in document_list:
                    print(f"\n chunk: {chunk}")
                    response = self.ws_client.index_documents(
                        content_source_id=self.ws_source, documents=chunk
                    )
                    print(f"\n response: {response}")
                    for each in response["results"]:
                        if each["errors"]:
                            item = list(
                                filter(
                                    lambda seq: self.filter_removed_item_by_id(
                                        seq, each["id"]
                                    ),
                                    document,
                                )
                            )
                            document.remove(item[0])
                            logger.error(
                                f"Error while indexing {each['id']}. Error: {each['errors']}"
                            )
                total_inserted_record_dict = self.get_records_by_types(document)
                for type, count in total_records_dict.items():
                    self.logger.info(
                        f"Total {total_inserted_record_dict[type]} {type} indexed out of {count}."
                    )
            else:
                self.logger.info(f"{param_name} are up-to-date to the worplace.")
            self.logger.info(f"Successfully indexed the {param_name} to the workplace")

        except Exception as exception:
            self.logger.exception(
                f"Error while indexing the {param_name} to the workplace. Error: {exception}"
            )
            self.is_error = True

    def get_records_by_types(self, document):
        """This method is used to for grouping the document based on their type
        :param document: Document to be indexed
        Returns:
             df_dict: dictonary of type with its count
        """
        df = pd.DataFrame(document)
        df_size = df.groupby("type").size()
        df_dict = df_size.to_dict()
        return df_dict

    def index_users(self, is_error_shared):
        """This method is used to index the user chats into Workplace Search.
        :param is_error_shared: List of boolean values denoting error encountered in each process
        :param user_drive: dictonary of dictonary
        """
        logger.info(
            "Started indexing user chat, meeting chat, attachments, tabs and meeting recoding"
        )
        global gv_start_time, gv_start_time
        storage_with_collection = {"global_keys": [], "delete_keys": []}
        ids_collection = {}
        doc_ids_storage = []
        try:
            # Logic to read data from ms_teams_user_chat_doc_ids.json file.
            if (
                os.path.exists(constant.USER_DEINDEXING_PATH)
                and os.path.getsize(constant.USER_DEINDEXING_PATH) > 0
            ):
                with open(constant.USER_DEINDEXING_PATH) as ids_store:
                    try:
                        ids_collection = json.load(ids_store)
                        ids_collection["global_keys"] = (
                            ids_collection.get("global_keys") or []
                        )
                        doc_ids_storage = ids_collection.get("global_keys")
                    except ValueError as exception:
                        logger.exception(
                            "Error while parsing the json file of the ids store from path: %s. Error: %s"
                            % (constant.USER_CHAT_DEINDEXING_PATH, exception)
                        )
            storage_with_collection["delete_keys"] = copy.deepcopy(
                ids_collection.get("global_keys")
            )
            users = ZoomUsers(
                self.access_token, logger, self.worker_process, self.get_schema_fields
            )
            documents, self.is_error = users.get_users_documents(
                doc_ids_storage, self.user_data
            )
            if "users" in self.objects:
                # self.bulk_index_document(documents, constant.USERS)
                self.bulk_index_document(documents, constant.USERS)
                logger.info("indexed USERS successfully")
            if not self.is_error:
                logger.info("Starting indexing of meetings.")
                meeting_documents, self.is_error = users.get_meetings(
                    self.user_data,
                    doc_ids_storage,
                    gv_start_time,
                    gv_end_time,
                )
                if "meetings" in self.objects:
                    self.bulk_index_document(meeting_documents, constant.MEETINGS)
                    logger.info("indexed MEETINGS successfully")
            # Logic to index user chat, meeting chat, attachments, tabs and meeting recoding into workplace search.
            # if self.permission:
            #     for id, mem_dict in user_permisssions.items():
            #         self.index_permissions(id, mem_dict)
            # Logic to update ms_teams_user_chat_doc_ids.json with latest data
            storage_with_collection["global_keys"] = list(doc_ids_storage)
            with open(constant.USER_DEINDEXING_PATH, "w") as f:
                try:
                    json.dump(storage_with_collection, f, indent=4)
                except ValueError as exception:
                    logger.warn(
                        "Error while adding ids to json file. Error: %s" % (exception)
                    )
        except Exception as exception:
            print_and_log(
                self.logger,
                "exception",
                "[Fail] Error while indexing user chat, meeting chat, attachments, tabs and meeting recoding. Error: %s"
                % (exception,),
            )
        is_error_shared.append(self.is_error)
        logger.info(
            "Completed indexing user chat, meeting chat, attachments, tabs and meeting recoding"
        )


def init_multiprocessing(zoom_access_token, user_data, type_, is_error_shared):
    indexer = Indexer(zoom_access_token, user_data)
    if type_ == "users":
        indexer.index_users(is_error_shared)


def get_partition_time(indexing_type, config, worker_process, check, type_):
    global gv_start_time, gv_end_time
    if indexing_type == "incremental":
        start_time, end_time = check.get_checkpoint(constant.CURRENT_TIME, type_)
        gv_end_time = datetime.strptime(end_time, constant.DATETIME_FORMAT)
    else:
        start_time = config.get("start_time")
        end_time = constant.CURRENT_TIME

    gv_start_time = datetime.strptime(start_time, constant.DATETIME_FORMAT)
    return gv_start_time, gv_end_time


def define_processes(
    zoom_access_token, worker_process, is_error_shared, jobs, user_data, job_type
):
    for num in range(0, len(user_data)):
        process = multiprocessing.Process(
            target=init_multiprocessing,
            args=(
                zoom_access_token,
                user_data[num],
                job_type,
                is_error_shared,
            ),
        )
        jobs.append(process)


def start_multiprocessing(indexing_type, config, zoom_access_token):
    worker_process = config.get("worker_process")
    is_error_shared_users = multiprocessing.Manager().list()
    # is_error_shared_meetings = multiprocessing.Manager().list()
    jobs = []
    obj_permissions_list = ["users", "meetings"]
    check = Checkpoint(logger)
    start_time, end_time = get_partition_time(
        indexing_type, config, worker_process, check, "users"
    )
    print(f"start={start_time} end={end_time}  : 236 indexer")
    logger.info(
        "Successfully fetched the checkpoint details: start_time: %s and end_time: %s, calling the indexing"
        % (start_time, end_time)
    )

    if "users" in config.get("objects") and "users" in obj_permissions_list:
        print("inside users if : 232 indexer")

        users = ZoomUsers(zoom_access_token, logger, worker_process)
        users_data = users.get_all_users(start_time, end_time)

        define_processes(
            zoom_access_token,
            worker_process,
            is_error_shared_users,
            jobs,
            users_data,
            job_type="users",
        )

    # if "meetings" in config.get("objects") and "meetings" in obj_permissions_list:
    #     print("inside meetings if : 276 indexer")
    #     meeting = ZoomUsers(zoom_access_token, logger, worker_process)
    #     meetings_data = meeting.get_meetings("RI9jz2goQo-ieZFtS6duGA", start_time, end_time)
    #     print(meetings_data)

    #     define_processes(
    #         zoom_access_token,
    #         worker_process,
    #         is_error_shared_meetings,
    #         jobs,
    #         meetings_data,
    #         job_type="meetings",
    #     )

    for job in jobs:
        job.start()
    for job in jobs:
        job.join()

    logger.info("Saving the checkpoints")
    if is_error_shared_users and True not in is_error_shared_users:
        end_time = end_time.strftime(constant.DATETIME_FORMAT)
        check.set_checkpoint(end_time, indexing_type, "users")
    # if is_error_shared_meetings and True not in is_error_shared_meetings:
    #     end_time = end_time.strftime(constant.DATETIME_FORMAT)
    #     check.set_checkpoint(end_time, indexing_type, "meetings")


def start(indexing_type):
    logger.info("Starting the indexing...")
    config = Configuration(logger).configurations
    docids_dir = os.path.dirname(constant.USER_DEINDEXING_PATH)
    if not os.path.exists(docids_dir):
        os.makedirs(docids_dir)
    while True:
        zoom_token = ZOOMAccessToken(logger)
        zoom_access_token = zoom_token.get_token()
        if indexing_type == "incremental":
            print(f"{indexing_type} : 269 indexer")
            interval = config.get("indexing_interval")
        else:
            interval = config.get("full_sync_interval")

        start_multiprocessing(indexing_type, config, zoom_access_token)
        # TODO: need to use schedule instead of time.sleep
        logger.info("Sleeping..")
        time.sleep(interval * 60)


if __name__ == "__main__":
    start("incremental")
