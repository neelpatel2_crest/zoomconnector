import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import json
import time
import multiprocessing
import src.logger_manager as log
from datetime import datetime
from src.base_class import BaseClass
from src.adapter import DEFAULT_SCHEMA
from src.checkpointing import Checkpoint
from src.configuration import Configuration
from src.utils import print_and_log
from src import constant
from src.zoom_access_token import ZOOMAccessToken
from src.zoom_users import ZoomUsers


logger = log.setup_logging("zoom_users_deindex")


class Deindexer(BaseClass):
    """This class is used to remove document from the workplace search"""

    def __init__(self, token):
        BaseClass.__init__(self, logger=logger)
        self.objects = self.configurations.get("objects")
        self.checkpoint = Checkpoint(logger)
        self.access_token = token

    def get_schema_fields(self, document_name):
        """Returns the schema of all the include_fields or exclude_fields specified in the configuration file.
        :param document_name: Document name from teams, channels, channel_messages, channel_tabs, channel_documents, calendar and user_chats
        Returns:
            schema: Included and excluded fields schema
        """
        fields = self.objects.get(document_name)
        adapter_schema = DEFAULT_SCHEMA[document_name]
        field_id = adapter_schema["id"]
        if fields:
            include_fields = fields.get("include_fields")
            exclude_fields = fields.get("exclude_fields")
            if include_fields:
                adapter_schema = {
                    key: val
                    for key, val in adapter_schema.items()
                    if val in include_fields
                }
            elif exclude_fields:
                adapter_schema = {
                    key: val
                    for key, val in adapter_schema.items()
                    if val not in exclude_fields
                }
            adapter_schema["id"] = field_id
        return adapter_schema

    def iterate_item_ntimes(
        self,
        live_doc_ids_storage,
        deleted_data,
        delete_keys_documents,
        global_keys_documents,
    ):
        # parent_items = list(filter(lambda seq: self.get_child_items(seq, parent_id), doc_ids_documents))
        for item in deleted_data:
            id = item["id"]
            parent_id = item["parent_id"]
            super_parent_id = item["super_parent_id"]
            type = item["type"]
            items_exists = list(
                filter(
                    lambda seq: self.check_item_isexists_in_livedata(seq, id),
                    live_doc_ids_storage,
                )
            )
            if len(items_exists) == 0 and type not in [constant.USERS]:
                delete_keys_documents.append(id)
                if item in global_keys_documents:
                    global_keys_documents.remove(item)

    def check_item_isexists_in_livedata(self, document_item, id):
        """The purpose of this method is to filter item from the live data.
        :param document_item: Pass user chat document
        :param parent_id: Pass id of user chat document
        """
        return document_item["id"] == id

    def delete_document(self, final_deleted_list):
        """This method will delete all the documents of specified ids from workplace search
        :param final_deleted_list: list of ids
        """
        step = constant.MAX_DELETED_DOCUMENT
        print("reached delete document")
        for index in range(0, len(final_deleted_list), constant.MAX_DELETED_DOCUMENT):
            final_list = final_deleted_list[index : index + step]
            try:
                # Logic to delete documents from the workplace search
                self.ws_client.delete_documents(
                    http_auth=self.ws_token,
                    content_source_id=self.ws_source,
                    document_ids=final_list,
                )
            except Exception as exception:
                self.logger.exception(
                    f"Error while deleting the documents to the workplace. Error: {exception}"
                )
                return []

    def deindexing_users(self):
        """The purpose of this method is to delete the teams related documents from the workplace search."""
        indexed_users = []
        delete_keys_documents = []
        global_keys_documents = []
        doc_id_data = {"global_keys": [], "delete_keys": []}
        logger.info(
            f"Started deindexing process of teams and it's objects on {datetime.now()}"
        )

        if (
            os.path.exists(constant.USER_DEINDEXING_PATH)
            and os.path.getsize(constant.USER_DEINDEXING_PATH) > 0
        ):
            with open(constant.USER_DEINDEXING_PATH, encoding="UTF-8") as ids_store:
                try:
                    indexed_users = json.load(ids_store)
                    indexed_users["delete_keys"] = (
                        indexed_users.get("delete_keys") or []
                    )
                    indexed_users["global_keys"] = (
                        indexed_users.get("global_keys") or []
                    )
                    global_keys_documents = indexed_users.get("global_keys")
                except ValueError as exception:
                    logger.exception(
                        "Error while reading teams data from the path: %s. Error: %s"
                        % (constant.USER_DEINDEXING_PATH, exception)
                    )
                # Fetching start datetime from the YML file becuase we have to we have to fetch all data and check respective document exists or not instead of calling individual to improve performance
                start_time = self.configurations.get("start_time")
                deleted_data = indexed_users["delete_keys"]
                users = ZoomUsers(
                    self.access_token,
                    logger,
                    self.worker_process,
                    self.get_schema_fields,
                )
                start_time = self.configurations.get("start_time")
                start_time = datetime.strptime(start_time, constant.DATETIME_FORMAT)
                end_time = datetime.strptime(
                    constant.CURRENT_TIME, constant.DATETIME_FORMAT
                )

                users_data = users.get_all_users(start_time, end_time)
                doc_ids_storage = []
                for user in users_data:
                    documents, self.is_error = users.get_users_documents(
                        doc_ids_storage, user
                    )

                self.iterate_item_ntimes(
                    doc_ids_storage,
                    deleted_data,
                    delete_keys_documents,
                    global_keys_documents,
                )
                final_deleted_list = list(delete_keys_documents)
                self.delete_document(final_deleted_list)

                # Logic to update the ms_teams_channel_chat_doc_ids.json file with latest data
                doc_id_data["global_keys"] = list(global_keys_documents)
                with open(constant.USER_DEINDEXING_PATH, "w", encoding="UTF-8") as f:
                    try:
                        json.dump(doc_id_data, f, indent=4)
                    except ValueError as exception:
                        logger.exception(
                            "Error while adding ids to json file. Error: %s"
                            % (exception)
                        )
        else:
            logger.info(
                "No records are present to check for deindexing teams and it's objects"
            )
        logger.info(
            f"Completed deindexing process of teams and it's objects on {datetime.now()}"
        )


def start_multiprocessing(job_name, access_token):
    deindexer = Deindexer(access_token)
    if job_name == "users":
        deindexer.deindexing_users()
    # if job_name == "user_chats":
    #     deindexer.deindexing_user_chat()
    # if job_name == "calendar":
    #     deindexer.deindexing_calendar_chat()


def start():
    logger.info("Starting the deindexing...")
    deindexing_processes = []
    data = Configuration(logger).configurations
    objects = data.get("objects")
    zoomTokenObj = ZOOMAccessToken(logger)
    zoomToken = zoomTokenObj.get_token()
    obj_permissions_list = ["users"]
    # obj_permissions_list = ["teams", "channels", "channel_messages", "channel_tabs", "channel_documents"]
    obj_permissions_list = ["users"]
    while True:
        if any(obj in objects for obj in obj_permissions_list):
            p1 = multiprocessing.Process(
                target=start_multiprocessing,
                args=(
                    "users",
                    zoomToken,
                ),
            )
            deindexing_processes.append(p1)
        # if "user_chats" in objects:
        #     p2 = multiprocessing.Process(target=start_multiprocessing, args=("user_chats", access_token,))
        #     deindexing_processes.append(p2)
        # if "calendar" in objects:
        #     calendar_access_token = token.get_token(is_aquire_for_client=True)
        #     p3 = multiprocessing.Process(target=start_multiprocessing, args=("calendar", calendar_access_token,))
        #     deindexing_processes.append(p3)

        # Logic to start the each job and run parallelly.
        for pro_item in deindexing_processes:
            pro_item.start()
        for pro_item in deindexing_processes:
            pro_item.join()

        interval = data.get("deletion_interval")
        # TODO: need to use schedule instead of time.sleep
        logger.info("Sleeping..")

        time.sleep(interval * 60)


if __name__ == "__main__":
    start()
