import http.client
import base64

conn = http.client.HTTPSConnection("zoom.us")

client_id = "BBmbdt1RWia4Nq0AOFUlA"
client_secret = "jXwShsJbeuQWGNeTlz7VzlEh79cQkuVA"
redirect_uri = "https://oauth.pstmn.io/v1/callback"
authorization_code = "MLwO7x5lIJ_RI9jz2goQo-ieZFtS6duGA"
creds = client_id + ":" + client_secret
creds_bytes = creds.encode("ascii")
base64_bytes = base64.b64encode(creds_bytes)
base64_creds = base64_bytes.decode("ascii")

payload = ""
headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": f"Basic {base64_creds}",
}

conn.request(
    "POST",
    f"/oauth/token?grant_type=authorization_code&code={authorization_code}&redirect_uri={redirect_uri}",
    payload,
    headers,
)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
