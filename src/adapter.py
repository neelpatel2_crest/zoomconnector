# WS : ZOOM
DEFAULT_SCHEMA = {
    "users": {
        "id": "id",
        "title": "first_name",
        "email": "email",
        "created_at": "created_at",
        "status": "status",
    },
    "meetings": {
        "id": "id",
        "user_id": "host_id",
        "start_time": "start_time",
        "topic": "topic",
        "meeting_type": "type",
        "duration": "duration",
        "join_url": "join_url",
    },
}
