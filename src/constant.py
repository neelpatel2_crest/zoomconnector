# Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
# or more contributor license agreements. Licensed under the Elastic License
# 2.0; you may not use this file except in compliance with the Elastic License
# 2.0.

import os
import datetime

BASE_URL = "api.zoom.us"
GENERATE_ACCESS_TOKEN_BASE_URL = "zoom.us"
CONFIG_FILE = os.path.join(os.path.dirname(__file__), "../zoom.yml")
CHECKPOINT_PATH = os.path.join(os.path.dirname(__file__), "checkpoint.json")
USERS = "Users"
MEETINGS = "Meetings"
SCOPE = [
    "User.Read.All",
    "TeamMember.Read.All",
    "ChannelMessage.Read.All",
    "Chat.Read",
    "Chat.ReadBasic",
    "Calendars.Read",
]
DOCUMENT_LIMIT = 100
PAGE_SIZE = 30
CURRENT_TIME = (datetime.datetime.utcnow()).strftime("%Y-%m-%dT%H:%M:%SZ")
DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
USER_DEINDEXING_PATH = os.path.join(
    os.path.dirname(__file__), "..", "doc_ids", "zoom_user_doc_ids.json"
)
MAX_DELETED_DOCUMENT = 100
MEETING_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"
USER_MEETING_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"
