# Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
# or more contributor license agreements. Licensed under the Elastic License
# 2.0; you may not use this file except in compliance with the Elastic License
# 2.0.

from src.base_class import BaseClass
from src.constant import GENERATE_ACCESS_TOKEN_BASE_URL
import http.client
import base64
import json


class ZOOMAccessToken(BaseClass):
    """This class is used to generate the access token to call the MS Graph APIs."""

    def __init__(self, logger):
        self.logger = logger
        BaseClass.__init__(self, logger=logger)

    def get_token(self):
        """Generates the access token to call MS Graph APIs
            :param is_aquire_for_client: Pass True if want to acquire token by using client_id, tenant_id and secret_key
        Returns:
            access_token: Access token for authorization
        """
        self.logger.info(
            f"Generating the access token for the client ID: {self.client_id}..."
        )

        try:
            conn = http.client.HTTPSConnection(GENERATE_ACCESS_TOKEN_BASE_URL)
            creds = self.client_id + ":" + self.client_secret
            creds_bytes = creds.encode("ascii")
            base64_bytes = base64.b64encode(creds_bytes)
            base64_creds = base64_bytes.decode("ascii")

            payload = ""
            headers = {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": f"Basic {base64_creds}",
            }

            conn.request(
                "POST",
                f"/oauth/token?grant_type=authorization_code&code={self.authorization_code}&redirect_uri={self.redirect_uri}",
                payload,
                headers,
            )
            res = conn.getresponse()
            data = res.read()
            json_data = data.decode("utf-8").replace("'", '"')
            json_data = json.loads(json_data)
            return json_data["access_token"]

        except Exception as exception:
            self.logger.exception(
                f"Error while generating the access token. Error: {exception}"
            )
            exit(0)
