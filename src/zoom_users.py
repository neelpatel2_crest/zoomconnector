import http.client
import json
import datetime
from src.utils import insert_document_into_doc_id_storage
from src.base_class import BaseClass
from src import constant
import math


class ZoomUsers(BaseClass):
    """This class is used to fetch all MS Teams users."""

    def __init__(self, token, logger, worker_process, get_schema_fields=None):
        self.access_token = token
        self.logger = logger
        self.get_schema_fields = get_schema_fields
        self.worker_process = worker_process

    def get_all_users(self, start_time, end_time):
        """This method fetched all MS Teams users.
        Returns:
            user_details: List of dictionaries containing user details.
        """
        user_details = []
        try:
            conn = http.client.HTTPSConnection("api.zoom.us")
            headers = {
                "authorization": f"Bearer {self.access_token}",
                "content-type": "application/json",
            }
            conn.request("GET", "/v2/users/", headers=headers)
            user_response = conn.getresponse()
            if user_response and user_response.status == 200:

                response_data = user_response.read()
                decode_data = response_data.decode("utf-8").replace("'", '"')
                user_data = json.loads(decode_data)
            else:
                self.logger.error("Error while fetching users from Zoom platform")
        except Exception as exception:
            self.logger.exception(exception)

        counter = 0
        mid_doc = []
        mid_index = 0
        num = 0

        total_data = len(user_data["users"])

        limit = math.floor((total_data / self.worker_process))

        if total_data < self.worker_process:
            num = total_data
        else:
            num = self.worker_process

        final_doc = [[]] * num

        for user in user_data["users"]:
            temp = datetime.datetime.strptime(
                user["created_at"], constant.DATETIME_FORMAT
            )
            if temp >= start_time and temp <= end_time:
                counter = counter + 1
                if counter < limit:
                    mid_doc.append(user)
                else:
                    mid_doc.append(user)
                    final_doc[mid_index] = final_doc[mid_index] + mid_doc
                    if mid_index < num - 1:
                        mid_index = mid_index + 1
                    else:
                        mid_index = 0
                    counter = 0
                    mid_doc = []
        print(final_doc)
        return final_doc

    def get_users_documents(self, doc_ids_storage, user_data_list):
        error = False
        document = []
        self.logger.info("Fetching users")
        user_schema = self.get_schema_fields("users")

        if user_data_list:
            self.logger.info(
                "Fetched the user. Attempting to extract Details of the user."
            )
            count = 0
            for user in user_data_list:

                insert_document_into_doc_id_storage(
                    doc_ids_storage, user["id"], constant.USERS, "", ""
                )
                user_dict = {"type": constant.USERS}
                for ws_field, zoom_fields in user_schema.items():
                    user_dict[ws_field] = user[zoom_fields]
                user_dict["title"] = user["first_name"]
                user_dict[
                    "body"
                ] = f"{user['first_name']} {user['last_name']} - {user['status']}"
                user_dict["url"] = user["email"]
                # if self.permission:
                #     user_dict["_allow_permissions"] = [val["id"]]
                document.append(user_dict)
                count += 1
            self.logger.info(f"fetched {count} number of Users.")
        return document, error

    def get_meetings_from_userid(self, userid, start_time, end_time):
        self.logger.info("Inside get_meetings function")
        meetings_for_user = None
        try:
            conn = http.client.HTTPSConnection("api.zoom.us")
            headers = {
                "authorization": f"Bearer {self.access_token}",
                "content-type": "application/json",
            }
            conn.request("GET", "/v2/users/" + userid + "/meetings", headers=headers)
            user_response = conn.getresponse()

            if user_response and user_response.status == 200:
                print(f" meeting response status {user_response.status} ")
                response_data = user_response.read()
                decode_data = response_data.decode("utf-8").replace("'", '"')
                meetings_for_user = json.loads(decode_data)
                self.logger.info(f"Fetched the meetings for {userid}.")
            else:
                self.logger.error("Error while fetching users from Zoom platform")
        except Exception as exception:
            self.logger.exception(exception)

        final_docc = []
        meeting_types = ["scheduled", "live", "upcoming"]
        for meeting in meetings_for_user["meetings"]:
            temp = datetime.datetime.strptime(
                meeting["created_at"], constant.DATETIME_FORMAT
            )
            if temp >= start_time and temp <= end_time:
                meeting["type"] = meeting_types[meeting["type"]]
                final_docc.append(meeting)
        # print(f"\n user meetings: {final_docc} \n")
        return final_docc

    def get_meetings(self, user_data, doc_ids_storage, start_time, end_time):
        error = False
        meeting_schema = self.get_schema_fields("meetings")
        docc = []
        count = 0
        print(user_data)
        for user in user_data:
            self.logger.info(f"Attempting to extract meetings for {user}.")
            meetings_list = self.get_meetings_from_userid(
                user["id"], start_time, end_time
            )
            for meeting in meetings_list:
                insert_document_into_doc_id_storage(
                    doc_ids_storage, meeting["id"], constant.MEETINGS, user["id"], ""
                )
                user_dict = {"type": constant.MEETINGS}
                for ws_field, zoom_fields in meeting_schema.items():
                    user_dict[ws_field] = meeting[zoom_fields]
                user_dict["title"] = meeting["topic"]
                user_dict["body"] = f"{meeting['topic']}"
                user_dict["url"] = meeting["join_url"]
                docc.append(user_dict)
                count += 1

        self.logger.info(f"\n fetched {count} number of Meetings.")
        #    print(f"all meetings: {docc} \n")
        return docc, error
